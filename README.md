# Pravallika Naraharasetty

## Masters in Computer Science

**Are you interested in BMI? And why?**

Yes, I am interested in BMI because It helps doctors give the right treatments to each person, finds new cures faster, and makes sure everyone stays healthy by using cool technology. It also helps to track and monitor the spread of diseases by analyzing health data.

**What is your technical background?**

Yes, I am from a Computer Science background, and I do have programming experience. I'm quite familiar with languages like Java and Python. While I can handle statistical analysis to some extent, it's not my strongest suit. I don't have experience in BMI projects

**What most concerns you about this course?**

I anticipate that some of the more complex statistical concepts in this course could be challenging for me. Also, the course covers both medical and technical stuff, I might need to work extra hard to understand everything really well.

**What do you expect to get out of this course?**

I hope that I will get a basic understanding of how Big Data analytics can be applied in Biomedical informatics. By the end of the course, I hope I'll be good at using data techniques to find important information and help make progress in the field.

 **Are there things that will help you succeed in this course?**

 Maintaining a good understanding of fundamental ideas, being attentive and asking questions, and cooperating with peers are all necessary steps for success in this course.

 **Is there anything else you would like the instructor to know?**
 
 No, there's nothing specific I need to share.
